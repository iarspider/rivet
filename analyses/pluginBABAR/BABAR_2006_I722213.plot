BEGIN PLOT /BABAR_2006_I722213/d01-x01-y01
Title=$K^0_S\Lambda^0$ mass distribution in $\Lambda_c^+\to\Lambda^0K^+K^0_S$
XLabel=$m_{K^0_S\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722213/d01-x01-y02
Title=$K^0_SK^+$ mass distribution in $\Lambda_c^+\to\Lambda^0K^+K^0_S$
XLabel=$m_{K^0_SK^+}-m_{K^0_S}-m_{K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722213/d01-x01-y03
Title=$K^+\Lambda^0$ mass distribution in $\Lambda_c^+\to\Lambda^0K^+K^0_S$
XLabel=$m_{K^+\Lambda^0}-m_{K^+}-m_\Lambda^0$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2006_I722213/d02-x01-y01
Title=Decay angle for $\Lambda^+_c\to\Xi(1690)^0K^+$ with $\Xi(1690)^0\to\Lambda^0K^0_S$ (no interference corr)
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722213/d02-x01-y02
Title=Decay angle for $\Lambda^+_c\to\Xi(1690)^0K^+$ with $\Xi(1690)^0\to\Lambda^0K^0_S$ (interference corr)
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
LogY=0
END PLOT
