BEGIN PLOT /BABAR_2009_I827787/d01-x01-y01
Title=$\langle m_X\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X\rangle$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d01-x01-y02
Title=$\langle m_X^2\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d01-x01-y03
Title=$\langle m_X^3\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X^3\rangle$ [$\text{GeV}^3$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d01-x01-y04
Title=$\langle m_X^4\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X^4\rangle$ [$\text{GeV}^4$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d01-x01-y05
Title=$\langle m_X^5\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X^5\rangle$ [$\text{GeV}^5$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d01-x01-y06
Title=$\langle m_X^6\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle m_X^6\rangle$ [$\text{GeV}^6$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d02-x01-y01
Title=$\langle n_X^2\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle n_X^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d02-x01-y02
Title=$\langle n_X^4\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle n_X^4\rangle$ [$\text{GeV}^4$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I827787/d02-x01-y03
Title=$\langle n_X^6\rangle$ in $B\to X_c\ell^-\bar\nu_\ell$
XLabel=$p^*_{\ell,\min}$
YLabel=$\langle n_X^6\rangle$ [$\text{GeV}^6$]
LogY=0
END PLOT
