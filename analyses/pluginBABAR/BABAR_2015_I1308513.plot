BEGIN PLOT /BABAR_2015_I1308513/d01-x01-y01
Title=$J/\psi\phi$ mass spectrum in $B^{+,0}\to J/\psi \phi K^{+,0}$
XLabel=$m_{J/\psi\phi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
