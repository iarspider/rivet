Name: BELLE_2004_I658085
Year: 2004
Summary: Mass distributions in $B\to D^* n\pi$ decays
Experiment: BELLE
Collider: KEKB
InspireID: 658085
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 70 (2004) 111103
Description:
  'Measurement of mass distributions of the pions in the decays $B\to D^* n\pi$, where all the pions are charged.
   The data were read from, the plots in the papers but are corrected for efficiency.'
ValidationInfo:
  'Herwig 7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2004ahd
BibTeX: '@article{Belle:2004ahd,
    author = "Majumder, G. and others",
    collaboration = "Belle",
    title = "{Observation of B0 ---\ensuremath{>} D*-(5 pi)+, B+ ---\ensuremath{>} D*-(4 pi)++ and B+ ---\ensuremath{>} anti-D*0(5 pi)+}",
    eprint = "hep-ex/0409008",
    archivePrefix = "arXiv",
    reportNumber = "KEK-PREPRINT-2004-45, BELLE-PREPRINT-2004-25",
    doi = "10.1103/PhysRevD.70.111103",
    journal = "Phys. Rev. D",
    volume = "70",
    pages = "111103",
    year = "2004"
}
'
