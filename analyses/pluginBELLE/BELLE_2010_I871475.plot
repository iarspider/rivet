BEGIN PLOT /BELLE_2010_I871475/d01-x01-y01
Title=$m^2_{K^+\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d01-x01-y02
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d01-x01-y03
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2010_I871475/d02-x01-y01
Title=$m^2_{K^+\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d02-x01-y02
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d02-x01-y03
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2010_I871475/d03-x01-y01
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($0.60<m^2_{K^+\pi^+\pi^-}<1.46\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x01-y02
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($1.46<m^2_{K^+\pi^+\pi^-}<2.32\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x01-y03
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($2.32<m^2_{K^+\pi^+\pi^-}<3.18\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x01-y04
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($3.18<m^2_{K^+\pi^+\pi^-}<4.04\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x01-y05
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($4.04<m^2_{K^+\pi^+\pi^-}4.90\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x02-y01
Title=$m^2_{K^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($0.60<m^2_{K^+\pi^+\pi^-}<1.46\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x02-y02
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($1.46<m^2_{K^+\pi^+\pi^-}<2.32\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x02-y03
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($2.32<m^2_{K^+\pi^+\pi^-}<3.18\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x02-y04
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($3.18<m^2_{K^+\pi^+\pi^-}<4.04\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d03-x02-y05
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to J/\psi K^+\pi^+\pi^-$ ($4.04<m^2_{K^+\pi^+\pi^-}4.90\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2010_I871475/d04-x01-y01
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($0.60<m^2_{K^+\pi^+\pi^-}<1.00\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x01-y02
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.00<m^2_{K^+\pi^+\pi^-}<1.40\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x01-y03
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.40<m^2_{K^+\pi^+\pi^-}<1.80\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x01-y04
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.80<m^2_{K^+\pi^+\pi^-}<2.20\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x01-y05
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($2.20<m^2_{K^+\pi^+\pi^-}2.60\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x02-y01
Title=$m^2_{K^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($0.60<m^2_{K^+\pi^+\pi^-}<1.00\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x02-y02
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.00<m^2_{K^+\pi^+\pi^-}<1.40\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x02-y03
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.40<m^2_{K^+\pi^+\pi^-}<1.80\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x02-y04
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($1.80<m^2_{K^+\pi^+\pi^-}<2.20\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I871475/d04-x02-y05
Title=$m^2_{\pi^+\pi^-}$  in $B^+\to \psi(2S) K^+\pi^+\pi^-$ ($2.20<m^2_{K^+\pi^+\pi^-}2.60\,\text{GeV}^2$)
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
