BEGIN PLOT /BELLE_2013_I1247463/d01-x01-y01
Title=Maximum $\Upsilon(1S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(1S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(1S)\pi^0,\max}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(1S)\pi^0,\max}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d01-x01-y02
Title=Maximum $\pi^0\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(1S)\pi^0\pi^0$
XLabel=$m_{pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d01-x01-y03
Title=Minimum $\Upsilon(1S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(1S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(1S)\pi^0,\min}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(1S)\pi^0,\min}$ [GeV]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2013_I1247463/d02-x01-y01
Title=Maximum $\Upsilon(2S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(2S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(2S)\pi^0,\max}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(2S)\pi^0,\max}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d02-x01-y02
Title=Maximum $\pi^0\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(2S)\pi^0\pi^0$
XLabel=$m_{pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d02-x01-y03
Title=Minimum $\Upsilon(2S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(2S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(2S)\pi^0,\min}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(2S)\pi^0,\min}$ [GeV]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2013_I1247463/d03-x01-y01
Title=Maximum $\Upsilon(3S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(3S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(3S)\pi^0,\max}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(3S)\pi^0,\max}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d03-x01-y02
Title=Maximum $\pi^0\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(3S)\pi^0\pi^0$
XLabel=$m_{pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247463/d03-x01-y03
Title=Minimum $\Upsilon(3S)\pi^0$ mass in $\Upsilon(5S)\to\Upsilon(3S)\pi^0\pi^0$
XLabel=$m_{\Upsilon(3S)\pi^0,\min}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Upsilon(3S)\pi^0,\min}$ [GeV]
LogY=0
END PLOT
