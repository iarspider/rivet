Name: BESIII_2018_I1674528
Year: 2018
Summary: Positron momentum spectrum in semileptonic $\Lambda_c^+$ decay
Experiment: BESIII
Collider: BEPC
InspireID: 1674528
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 121 (2018) 25, 251801
RunInfo: e+e -> hadrons
Beams: [e+, e-]
Energies: [4,6]
Description:
'Measurement of the positron momentum spectrum in the lab frame for
 semileptonic $\Lambda_c^+$ decay.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2018mug
BibTeX: '@article{BESIII:2018mug,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurement of the absolute branching fraction of the inclusive semileptonic $\Lambda_c^+$ decay}",
    eprint = "1805.09060",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.121.251801",
    journal = "Phys. Rev. Lett.",
    volume = "121",
    number = "25",
    pages = "251801",
    year = "2018"
}
'
