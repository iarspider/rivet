BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y01
Title=$K^0_SK^-$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^0_SK^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y02
Title=$K^0_S\pi^+_1$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^0_S\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y03
Title=$K^-\pi^+_2$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^-\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y04
Title=$K^0_S\pi^+_2$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^0_S\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y05
Title=$K^-\pi^+_1$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^-\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y06
Title=$K^0_SK^-\pi^+_1$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^0_SK^-\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y07
Title=$K^0_SK^-\pi^+_2$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^0_SK^-\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y08
Title=$\pi^+_1\pi^+_2$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{\pi^+_1\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_1\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1845444/d01-x01-y09
Title=$K^-\pi^+_1\pi^+_2$ mass distribution in $D^+_s\to K^0_SK^-\pi^+\pi^+$
XLabel=$m_{K^-\pi^+_1\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_1\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
