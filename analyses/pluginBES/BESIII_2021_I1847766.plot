BEGIN PLOT /BESIII_2021_I1847766/d01-x01-y01
Title=$\sigma(e^+e^-\to p\bar{p})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to p\bar{p})$/pb
END PLOT
