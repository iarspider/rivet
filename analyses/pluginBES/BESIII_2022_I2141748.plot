BEGIN PLOT /BESIII_2022_I2141748/d01-x01-y01
Title=$e^+e^-$ mass distribution in $\psi(2S)\to\eta_c e^+e^-$
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{e^+e^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
