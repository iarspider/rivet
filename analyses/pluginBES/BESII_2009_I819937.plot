BEGIN PLOT /BESII_2009_I819937/d01-x01-y01
Title=$p\pi^0$ mass distribution in $J/\psi\to p\bar{p}\pi^0$
XLabel=$m_{p\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2009_I819937/d01-x01-y02
Title=$\bar{p}\pi^0$ mass distribution in $J/\psi\to p\bar{p}\pi^0$
XLabel=$m_{\bar{p}\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2009_I819937/dalitz
Title=Dalitz plot for  $J/\psi\to p\bar{p}\pi^0$
XLabel=$m^2_{p\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\pi^0}/{\rm d}m^2_{\bar{p}\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
