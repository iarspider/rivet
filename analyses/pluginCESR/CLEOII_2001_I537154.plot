BEGIN PLOT /CLEOII_2001_I537154/d01-x01-y01
Title=$K^-\pi^0$ mass distribution
XLabel=$m_{K^-\pi^0}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^0}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2001_I537154/d01-x02-y01
Title=$K^-\pi^+$ mass distribution
XLabel=$m_{K^-\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2001_I537154/d01-x03-y01
Title=$\pi^+\pi^0$ mass distribution
XLabel=$m_{\pi^+\pi^0}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
