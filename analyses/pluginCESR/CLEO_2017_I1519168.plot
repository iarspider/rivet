BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y01
Title=Minimum $\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\min(m^2_{\pi^+\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\min(m^2_{\pi^+\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y02
Title=Maximum $\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\max(m^2_{\pi^+\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\max(m^2_{\pi^+\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y04
Title=$\pi^+\pi^+$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$m^2_{\pi^+\pi^+}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^+}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y05
Title=Minimum $\pi^+\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\min(m^2_{\pi^+\pi^+\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\min(m^2_{\pi^+\pi^+\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y06
Title=Maximum $\pi^+\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\max(m^2_{\pi^+\pi^+\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\max(m^2_{\pi^+\pi^+\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y07
Title=Minimum $\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\min(m^2_{\pi^+\pi^-\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\min(m^2_{\pi^+\pi^-\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d01-x01-y08
Title=Maximum $\pi^+\pi^-\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\max(m^2_{\pi^+\pi^-\pi^-})$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\max(m^2_{\pi^+\pi^-\pi^-})$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+K^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+K^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y02
Title=$K^+\pi^+$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^+}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^+}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y03
Title=$K^+\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y04
Title=$K^-\pi^+$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^-\pi^+}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^-\pi^+}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y05
Title=$K^-\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^-\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^-\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d02-x01-y06
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d03-x01-y01
Title=$K^+K^-\pi^+$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+K^-\pi^+}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+K^-\pi^+}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d03-x01-y02
Title=$K^+K^-\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+K^-\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+K^-\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d03-x01-y03
Title=$K^+\pi^+\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2017_I1519168/d03-x01-y04
Title=$K^-\pi^+\pi^-$ mass distribution in $D^0\to K^+K^-\pi^+\pi^-$
XLabel=$m^2_{K^-\pi^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^-\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
