Name: H1_2006_I699835
Year: 2006
Summary: Measurement of Event Shape Variables in Deep-Inelastic Scattering at HERA
Experiment: H1
Collider: HERA
InspireID: 699835
Status: VALIDATED
Reentrant: True
Authors:
 - Alejandro Basilio Galvan <alejandrobasilio7@gmail.com>
 - Hannes Jung <hannes.jung@desy.de>
References:
 - 'Eur.Phys.J.C 46 (2006) 343-356'
 - 'DOI:10.1140/epjc/s2006-02493-x'
 - 'arXiv:hep-ex/0512014v1'
Beams: [e+, p+]
Energies: [[27.6, 820],[27.6,920]]
Description: 
  'Deep-inelastic ep scattering data taken with the H1 detector at HERA and corresponding to an integrated luminosity of $106 pb^{-1}$ are used to study the differential distributions of event shape variables. These include thrust, jet broadening, jet mass and the C-parameter. The four-momentum transfer Q is taken to be the relevant energy scale and ranges between 14 GeV and 200 GeV.'
ValidationInfo:
  'For the validation of the code, the resulting histograms were compared to those obtained with RivetHZTool, an older, Fortran-based analysis package. They were compared to the experimental data as well.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: H1:2005zsk
BibTeX: '@article{H1:2005zsk,
    author = "Aktas, A. and others",
    collaboration = "H1",
    title = "{Measurement of event shape variables in deep-inelastic scattering at HERA}",
    eprint = "hep-ex/0512014",
    archivePrefix = "arXiv",
    reportNumber = "DESY-05-225",
    doi = "10.1140/epjc/s2006-02493-x",
    journal = "Eur. Phys. J. C",
    volume = "46",
    pages = "343--356",
    year = "2006"
}'

