BEGIN PLOT /E691_1992_I342947
LogY=0
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y01
Title=$\pi^+\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y02
Title=$K^-\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/dalitz1
Title=Dalitz plot for $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{K^-\pi^+}$ [$\rm{GeV}^{-4}$]
END PLOT


BEGIN PLOT /E691_1992_I342947/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y04
Title=$K^-\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y05
Title=$\pi^0\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{\pi^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/dalitz2
Title=Dalitz plot for $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{K^-\pi^0}$ [$\rm{GeV}^{-4}$]
END PLOT


BEGIN PLOT /E691_1992_I342947/d01-x01-y07
Title=$\bar{K}^0\pi^+$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y06
Title=$\bar{K}^0\pi^-$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/d01-x01-y08
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /E691_1992_I342947/dalitz3
Title=Dalitz plot for $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\bar{K}^0\pi^+}/{\rm d}m^2_{\bar{K}^0\pi^-}$ [$\rm{GeV}^{-4}$]
END PLOT
