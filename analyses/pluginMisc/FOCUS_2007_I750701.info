Name: FOCUS_2007_I750701
Year: 2007
Summary: Dalitz plot analysis of $D^+\to K^-\pi^+\pi^+$
Experiment: FOCUS
Collider:  Fermilab
InspireID: 750701
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 653 (2007) 1-11
RunInfo: Any process producing D+ -> K- pi+ pi+
Description:
  'Measurement of the mass distributions in the decay $D^+\to K^-\pi^+\pi^+$.
   The data were read from the plots in the paper.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: FOCUS:2007mcb
BibTeX: '@article{FOCUS:2007mcb,
    author = "Link, J. M. and others",
    collaboration = "FOCUS",
    title = "{Dalitz plot analysis of the $D^{+} \to K^{-} \pi^{+} \pi^{+}$ decay in the FOCUS experiment}",
    eprint = "0705.2248",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "FERMILAB-PUB-07-127-E, FERMILAB-PUB-07-127E",
    doi = "10.1016/j.physletb.2007.06.070",
    journal = "Phys. Lett. B",
    volume = "653",
    pages = "1--11",
    year = "2007"
}
'
