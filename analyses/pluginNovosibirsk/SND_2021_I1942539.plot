BEGIN PLOT /SND_2021_I1942539/d01-x01-y01
Title=$\sigma(e^+e^-\to \eta\eta\gamma)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \eta\eta\gamma)$/pb
LegendXPos=0.1
LogY=0
END PLOT
