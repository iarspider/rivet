BEGIN PLOT /MARKII_1984_I195739/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-/K^+K^-$ with $|\cos\theta|<0.3$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-/K^+K^-)$ [nb]
LogY=1
END PLOT
