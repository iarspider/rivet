// -*- C++ -*-
#include "Rivet/Projections/InvisibleFinalState.hh"

namespace Rivet {


  CmpState InvisibleFinalState::compare(const Projection& p) const {
    const PCmp fscmp = mkNamedPCmp(p, "FS");
    if (fscmp != CmpState::EQ) return fscmp;
    const InvisibleFinalState& other = dynamic_cast<const InvisibleFinalState&>(p);
    return cmp(_requirePromptness, other._requirePromptness) ||
           cmp(_allow_from_direct_tau, other._allow_from_direct_tau) ||
           cmp(_allow_from_direct_mu, other._allow_from_direct_mu);
  }



  void InvisibleFinalState::project(const Event& e) {
    const FinalState& fs = applyProjection<FinalState>(e, "FS");
    _theParticles.clear();
    auto veto = [&](const Particle& p){
      return p.isVisible() || (_requirePromptness && !p.isDirect(_allow_from_direct_tau, _allow_from_direct_mu));
    };
    std::remove_copy_if(fs.particles().begin(), fs.particles().end(), std::back_inserter(_theParticles), veto);
    MSG_DEBUG("Number of invisible final-state particles = " << _theParticles.size());
  }


}
